<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create colis</title>
</head>
<body>

	<form method="post" action="createcolis">
	    <fieldset>
	        <legend>Cr�ation colis</legend>
	        <p>Vous pouvez cr�er un colis.</p>
	
	        <label for="unicId">unic id <span class="requis">*</span></label>
	        <input type="text" id="unicId" name="unicId" value="" size="20" maxlength="60" />
	        <br />
	
	        <label for="origin">Origin <span class="requis">*</span></label>
	        <input type="text" id="origin" name="origin" value="" size="20" maxlength="20" />
	        <br />
	
	        <label for="destination">destination <span class="requis">*</span></label>
	        <input type="text" id="destination" name="destination" value="" size="20" maxlength="20" />
	        <br />
	
	        <label for="value">value </label>
	        <input type="text" id="value" name="value" value="" size="20" maxlength="20" />
	        <br />

	        <label for="weight">weight </label>
	        <input type="text" id="weight" name="weight" value="" size="20" maxlength="20" />
	        <br />
	
	        <input type="submit" value="Cr�er" class="sansLabel" />
	        <br />
	    </fieldset>
	</form>	

</body>
</html>