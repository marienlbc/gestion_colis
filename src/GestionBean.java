import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class GestionBean implements Gestion {

    @PersistenceContext
    private EntityManager em;

    public GestionBean() {
        super();
    }

    @Override
    public Colis CreateColis(String unicId, double weight, double value, String origin, String destination) {
        Colis c = new Colis(unicId, weight, value, origin, destination);
        System.out.println(c.toString());
        em.persist(c);
        return c;
    }

    @Override
    public SuiviColis CreateSuiviColis(String idSuivi, double latitude, double longitude, String location, String status, String idColis) {
        SuiviColis sc = new SuiviColis(idSuivi, latitude, longitude, location, status, idColis);
        em.persist(sc);
        return sc;
    }


    @Override
    public SuiviColis ModifiateSuiviColis(String idSuivi, double latitude, double longitude, String location, String status) {
        SuiviColis sc = em.find(SuiviColis.class, idSuivi);
        sc.setLatitude(latitude);
        sc.setLongitude(longitude);
        sc.setLocation(location);
        sc.setStatus(status);
        return sc;
    }
    
    @Override
	public List<Colis> listeColis(){
    	Query q = em.createNamedQuery("allPackets");
    	return q.getResultList();
    }
}
