

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ColisServlet
 */
@WebServlet("/createcolis")
public class ColisServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@EJB
	private Gestion gb;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ColisServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String unicId = request.getParameter("unicId");
        String destination = request.getParameter("destination");
        String value = request.getParameter("value");
        String origin = request.getParameter("origin");
        String weight = request.getParameter("weight");
        double val = Double.parseDouble(value);
        double wght = Double.parseDouble(weight);

        //(String unicId, double weight, double value, String origin, String destination)
        
        PrintWriter out = response.getWriter();
        System.out.println(request.getParameter("destination"));
        System.out.println(request.getParameter("value"));
        System.out.println(request.getParameter("origin"));
        System.out.println(request.getParameter("weight"));
        System.out.println(request.getParameter("unicId"));
        if (unicId != null && weight != null && value != null && origin != null & destination != null ) {
            Colis colis = gb.CreateColis(unicId,wght,val,origin,destination);

            request.setAttribute("colis", colis);
            List<Colis> liste = gb.listeColis();
            System.out.println("liste : " +liste.size());
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        } else {
            out.println("creation colis KO");
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
