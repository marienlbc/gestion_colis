import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

import java.io.Serializable;

@NamedQuery(name="allPackets", query="select c from Colis c")

@Entity
public class Colis implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    private String unicId;
    private double weight;
    private double value;
    private String origin;
    private String destination;

    public Colis() {
        super();
    }

    @Override
	public String toString() {
		return "Colis [unicId=" + unicId + ", weight=" + weight + ", value=" + value + ", origin=" + origin
				+ ", destination=" + destination + "]";
	}

	public Colis(String unicId, double weight, double value, String origin, String destination) {
        this.unicId = unicId;
        this.weight = weight;
        this.value = value;
        this.origin = origin;
        this.destination = destination;
    }

    public String getUnicId() { return unicId;}

    public void setUnicId(String unicId) {this.unicId = unicId;}

    public double getWeight() {return weight;}

    public void setWeight(double weight) {this.weight = weight;}

    public double getValue() {return value;}

    public void setValue(double value) {this.value = value;}

    public String getOrigin() {return origin;}

    public void setOrigin(String origin) {this.origin = origin;}

    public String getDestination() {return destination;}

    public void setDestination(String destination) {this.destination = destination;}
}
