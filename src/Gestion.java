import java.util.List;

public interface Gestion {

    Colis CreateColis(String unicId, double weight, double value, String origin, String destination);

    SuiviColis CreateSuiviColis(String idSuivi, double latitude, double longitude, String location, String status, String idColis);

    SuiviColis ModifiateSuiviColis(String idSuivi,double latitude, double longitude, String location, String status );

	List<Colis> listeColis();
}
