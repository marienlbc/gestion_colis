import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import java.io.Serializable;

@Entity
public class SuiviColis implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    private String idSuivi;
    private double latitude;
    private double longitude;
    private String location;
    private String status;
    String idColis;
    //@ManyToOne
    //private Colis colis;

    public SuiviColis() {
        super();
    }

    public SuiviColis(String idSuivi, double latitude, double longitude, String location, String status, String idColis) {
        this.idSuivi = idSuivi;
        this.latitude = latitude;
        this.longitude = longitude;
        this.location = location;
        this.status = status;
        this.idColis = idColis;
    }

    public String getIdSuivi() {return idSuivi;}

    public void setIdSuivi(String idSuivi) {this.idSuivi = idSuivi;}

    public double getLatitude() {return latitude;}

    public void setLatitude(double latitude) {this.latitude = latitude;}

    public double getLongitude() {return longitude;}

    public void setLongitude(double longitude) {this.longitude = longitude;}

    public String getLocation() {return location;}

    public void setLocation(String location) {this.location = location;}

    public String getStatus() {return status;}

    public void setStatus(String status) {this.status = status;}

    public String getIdColis() {return idColis;}

    public void setIdColis(String idColis) {this.idColis = idColis;}
}